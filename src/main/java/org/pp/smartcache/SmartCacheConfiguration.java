package org.pp.smartcache;

import org.pp.smartcache.collection.CollectionCacheableAspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SmartCacheConfiguration {

    @Autowired
    private ApplicationContext appContext;

    @Bean
    CollectionCacheableAspect collectionCacheableAspect() {
        CacheManager cacheManager = appContext.getBean(CacheManager.class);
        return new CollectionCacheableAspect(cacheManager);
    }
}
