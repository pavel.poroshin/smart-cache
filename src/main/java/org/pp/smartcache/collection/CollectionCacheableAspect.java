package org.pp.smartcache.collection;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CollectionCacheableAspect {
    private final CacheManager cacheManager;

    public CollectionCacheableAspect(@Autowired CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Pointcut("@annotation(org.pp.smartcache.collection.CollectionCacheable)")
    public void CollectionCacheCall() {
    }

    @Around("CollectionCacheCall()")
    public Object useCache(ProceedingJoinPoint pjp) throws Throwable {
        CollectionCacheableProcessor processor = new CollectionCacheableProcessor(cacheManager, pjp);
        return processor.process();
    }
}
