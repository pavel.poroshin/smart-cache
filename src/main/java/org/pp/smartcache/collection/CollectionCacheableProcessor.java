package org.pp.smartcache.collection;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.pp.smartcache.collection.CollectionCacheable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.function.Predicate.not;
import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.*;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class CollectionCacheableProcessor {
    Logger logger = LoggerFactory.getLogger(CollectionCacheableProcessor.class);

    private final ProceedingJoinPoint pjp;
    private final MethodSignature signature;

    private final Object[] args;
    private final String[] names;
    private final Cache cache;

    private final String keyTemplate;
    private final String resultKeyTemplate;
    private final String unlessTemplate;
    private final boolean skipUnlessInResult;
    private final boolean useKeyConstraint;

    private int collectionPos = -1;
    private final StandardEvaluationContext evaluationContext;


    public CollectionCacheableProcessor(CacheManager cacheManager, ProceedingJoinPoint pjp) {
        this.pjp = pjp;
        this.signature = (MethodSignature) pjp.getSignature();
        Method targetMethod = signature.getMethod();
        CollectionCacheable annotation = targetMethod.getAnnotation(CollectionCacheable.class);
        this.cache = cacheManager.getCache(annotation.cacheName());
        this.names = signature.getParameterNames();
        this.args = pjp.getArgs();

        this.keyTemplate = annotation.key();
        this.resultKeyTemplate = annotation.resultKey();
        this.unlessTemplate = annotation.unless();
        this.skipUnlessInResult = annotation.skipUnlessInResult();
        this.useKeyConstraint = annotation.useKeyConstraint();

        this.evaluationContext = initEvaluationContext();
    }

    public Object process() throws Throwable {
        if (isCollectionEmpty(args[collectionPos])) {
            return pjp.proceed(args);
        }

        var tuples = evaluateKeys(keyTemplate, (Collection<?>) args[collectionPos]);
        var itemsFromCache = getItemsFromCache(tuples);

        var newArgs = prepareArgsForRealMethod(itemsFromCache);
        var realMethodResult = callRealMethod(newArgs);

        var unlessFilteredResult = filterUnlessAndPutToCache((Collection<?>) realMethodResult, itemsFromCache);

        Class<?> realReturnClass = (realMethodResult == null) ? signature.getReturnType() : realMethodResult.getClass();
        return prepareResult(unlessFilteredResult, realReturnClass);
    }

    private Tuple[] evaluateKeys(String template, Collection<?> objects) {
        Tuple[] tuples = new Tuple[objects.size()];
        int count = 0;
        for(Object obj:objects) {
            evaluationContext.setVariable(names[collectionPos], obj);
            tuples[count++] = new Tuple(obj,
                    evaluateExpression(template, String.class), null);
        }
        return tuples;
    }

    private Tuple[] getItemsFromCache(Tuple[] tuples) {
        if(tuples == null) {
            return null;
        }
        for (Tuple tuple : tuples) {
            List cachedItems = cache.get(tuple.key, List.class);
            if(cachedItems !=null) {
                tuple.setValue(cachedItems);
                tuple.setAlreadyInCache(true);
            }
        }
        return tuples;
    }

    private Object[] prepareArgsForRealMethod(Tuple[] itemsFromCache) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        Object[] newArgs = new Object[args.length];
        for (int count = 0; count < args.length; count++) {
            if(count == collectionPos) {
                Class<?> paramClass = args[count].getClass();
                newArgs[count] = prepareCollectionArg(itemsFromCache, paramClass);
            } else {
                newArgs[count] = args[count];
            }
        }
        return newArgs;
    }

    private Object prepareCollectionArg(Tuple[] itemsFromCache, Class<?> collectionClass) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        Stream<?> newParamStream = Arrays.stream(itemsFromCache)
                .filter(not(Tuple::isAlreadyInCache))
                .map(Tuple::getArg);
        return streamToCollection(newParamStream, collectionClass);
    }

    private List<Tuple> filterUnlessAndPutToCache(Collection<?> realCallResult, Tuple[] tuples) {
        List<Tuple> tupleList = new ArrayList<>(Arrays.asList(tuples));
        Collection<?> realCall = ofNullable(realCallResult)
                .orElse(emptyList());

        var inputTupleMap = Arrays.stream(tuples)
                .collect(toMap(Tuple::getKey, identity()));

        for(Object obj: realCall) {
            String resultKey = evaluateResultKey(obj);
            if (isEmpty(resultKey)) {
                continue;
            }
            Tuple tupleByResultKey = inputTupleMap.get(resultKey);
            if (tupleByResultKey == null) {
                String message = format("ResultKey [%s] doesn't exist in the requested ones [%s]. Result key must be equal to key, otherwise it is impossible to get object from cache further.",
                        resultKey, keysToLogString(tuples));
                if(useKeyConstraint)
                    throw new IllegalStateException(message);
                else {
                    logger.warn(message);
                    tupleByResultKey = new Tuple(null, resultKey, null);
                    tupleList.add(tupleByResultKey);
                }
            }

            tupleByResultKey.setUnless(tupleByResultKey.isUnless() || isUnless(obj));
            tupleByResultKey.addValue(obj);
        }

        List<Tuple> result = new ArrayList<>();
        for(Tuple tuple: tupleList) {
            if(tuple.getValue() == null) {
                continue;
            }
            if(!tuple.isUnless() && !tuple.isAlreadyInCache) {
                cache.putIfAbsent(tuple.key, tuple.getValue());
            }
            if (tuple.unless && skipUnlessInResult) {
                continue;
            }
            result.add(tuple);
        }
        return result;
    }

    private String evaluateResultKey(Object obj) {
        evaluationContext.setVariable("result", obj);
        evaluationContext.setVariable(names[collectionPos], args[collectionPos]);
        return evaluateExpression(resultKeyTemplate, String.class);
    }

    private boolean isUnless(Object obj) {
        if(isEmpty(unlessTemplate)) {
            return false;
        }

        evaluationContext.setVariable("result", obj);
        Boolean isUnless = evaluateExpression(unlessTemplate, Boolean.class);
        if(isUnless) {
            logger.debug(format("The object [%s] is skipped to store in the cache because of 'unless' condition.", obj));
        }
        return isUnless;
    }

    private StandardEvaluationContext initEvaluationContext() {
        StandardEvaluationContext context = new StandardEvaluationContext();
        for (byte paramCount = 0; paramCount < signature.getParameterNames().length; paramCount++) {
            String curParamName = names[paramCount];
            Object curArg = args[paramCount];
            Class<?> argClass = signature.getParameterTypes()[paramCount];

            if(Collection.class.isAssignableFrom(argClass)) {
                if(collectionPos >= 0) {
                    throw new IllegalArgumentException("Annotation @CollectionCacheable supports only one collection as function parameter. Otherwise use @Cacheable annotation instead of.");
                }
                collectionPos = paramCount;
            } else {
                context.setVariable(curParamName, ofNullable(curArg)
                        .map(Object::toString)
                        .orElse("null"));
            }
        }

        if( collectionPos < 0) {
            throw new IllegalArgumentException("There is no input params Collection, @Cacheable annotation must be used.");
        }
        return context;
    }

    private Object prepareResult(List<Tuple> result, Class<?> returnClass) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        Stream<?> resultStream = result.stream()
                .map(Tuple::getValue)
                .flatMap(Collection::stream);
        return streamToCollection(resultStream, returnClass);
    }

    private Collection<?> streamToCollection(Stream<?> stream, Class<?> clazz) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        Constructor<?> constructor = getDefaultConstructor(clazz);
        if(constructor != null) {
            List<?> list = stream.collect(Collectors.toList());
            return (Collection<?>) constructor.newInstance(list);
        } if (isList(clazz)) {
            return stream.collect(toList());
        } else if (isSet(clazz)) {
            return stream.collect(toSet());
        } else if (isCollection(clazz)) {
            return stream.collect(Collectors.toList());
        }
        throw new IllegalStateException("Annotation @CollectionCacheable supports only List and Set collections.");
    }

    private Constructor<?> getDefaultConstructor(Class<?> clazz) {
        try {
            return clazz.getConstructor(Collection.class);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }
    private boolean isSet(Class<?> clazz) {
        return Set.class.isAssignableFrom(clazz);
    }
    private boolean isCollection(Class<?> clazz) {
        return clazz == Collection.class;
    }

    private boolean isList(Class<?> clazz) {
        return List.class.isAssignableFrom(clazz);
    }

    private <T> T evaluateExpression(String template, Class<T> clazz) {
        ExpressionParser parser = new SpelExpressionParser();
        return parser.parseExpression(template)
                .getValue(evaluationContext, clazz);
    }

    private Object callRealMethod(Object[] args) throws Throwable {
        Collection<?> collectionArg = (Collection<?>) args[collectionPos];
        if(collectionArg == null || collectionArg.size() == 0) {
            return null;
        }
        return pjp.proceed(args);
    }

    private boolean isCollectionEmpty(Object obj) {
        Collection<?> collection = (Collection<?>) obj;
        return CollectionUtils.isEmpty(collection);
    }

    private String keysToLogString(Tuple[] tuples) {
        return Arrays.stream(tuples)
                .map(Tuple::getKey)
                .collect(joining(","));
    }

    static private class Tuple {
        private Object arg;
        private String key;
        private List<Object> value;
        private boolean unless = false;
        private boolean isAlreadyInCache = false;

        public Tuple(Object param, String key, List<Object> value) {
            this.arg = param;
            this.key = key;
            this.value = value;
        }

        public Object getArg() {return arg;}

        public String getKey() {return key;}

        public List<Object> getValue() {return value;}

        public void setArg(Object arg) {this.arg = arg;}

        public void setKey(String key) {this.key = key;}

        public void setValue(List<Object> value) {
            this.value = value;
        }
        public void addValue(Object obj) {
            if (this.value == null) {
                this.value = new ArrayList<>();
            }
            this.value.add(obj);
        }

        public void setUnless(boolean unless) { this.unless = unless;}

        public boolean isUnless() { return unless;}

        public boolean isAlreadyInCache() { return isAlreadyInCache;}

        public void setAlreadyInCache(boolean alreadyInCache) { isAlreadyInCache = alreadyInCache;}
    }
}
