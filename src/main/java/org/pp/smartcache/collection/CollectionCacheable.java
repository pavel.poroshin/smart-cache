package org.pp.smartcache.collection;

import org.springframework.cache.annotation.CacheConfig;

import java.lang.annotation.*;

/**
 * Annotation indicating that the result of invoking a method (or all methods
 * in a class) can be cached.
 *
 * <p>Each time an advised method is invoked, caching behavior will be applied.
 * The input params must contains the only collection among other params. Input
 * collection params must be any class extends {@link java.util.Collection}.
 * Return type must be {@link java.util.Collection}. Each time an advised method
 * is invoked, caching behavior will be applied, checking whether the objects are
 * already in cache for the given keys. If some object exists in cache, the
 * objects are excluded from the real method call. the Real method get collection
 * with out cached cache. Then real method call result unions with cached objects
 * with the same order as in input params. If collection param is null or empty,
 * the real method is called, cache is not used and result is not cached.
 *
 * @author Pavel Poroshin
 * @see CacheConfig
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
@Documented
public @interface CollectionCacheable {

    /**
     * Names of the caches in which method invocation results are stored.
     * <p>Names may be used to determine the target cache, matching
     * the qualifier value or bean name of a specific bean definition.
     * @see CacheConfig#cacheNames
     *
     * @return Return {@link java.lang.String}. Cache name in the current
     * cacheManager.
     */
    String cacheName();

    /**
     * Spring Expression Language (SpEL) expression for computing the key dynamically.
     * <p>Default is {@code ""}, meaning all method parameters are considered as a key,
     * <ul>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Arguments can be accessed by name if that information is available.</li>
     * <li>{@link #key} is calculated or each object in collection argument. Each
     * object accessible with the collection argument name. For instance
     * method(String str, List list) is called, where list contains
     * ["1", "2", "3", "4"] items. {@link #key} "#str + '_' + #list" is evaluated with
     * result set of keys ["str_1", "str_2","str_3","str_4",].</li>
     * </ul>
     *
     * @return Return {@link java.lang.String}. The is SPeL expression for evaluation
     * key for a getting objects in cache.
     */
    String key();

    /**
     * Spring Expression Language (SpEL) expression for computing the key dynamically.
     * <p>Default is {@code ""}, meaning all method parameters are considered as a key,
     * <ul>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Arguments can be accessed by name if that information is available.</li>
     * <li>{@link #key} is calculated or each object in result collection. Each
     * object accessible with "result" name. </li>
     * <li>{@link #key}  must be equal to {@link #resultKey} otherwise
     * {@link IllegalStateException} is thrown. </li>
     * </ul>
     *
     * @return Return {@link java.lang.String}. The is SPeL expression for evaluation
     * key on result objects to put object to cache. Result keys must be equals to
     * input key.
     */
    String resultKey();

    /**
     * {@link #useKeyConstraint} is boolean expression. if it is true, the {@link #key} and
     * {@link #resultKey} constrain validation takes place, otherwise only warning log message is printed
     * Default {@link #useKeyConstraint} is true.
     * @return Return {@link boolean}.
     */
    boolean useKeyConstraint() default true;

    /**
     * Spring Expression Language (SpEL) expression used to veto method caching.
     * <p>{@link #unless}, this expression is evaluated after the method
     * has been called and can therefore refer to the {@code result}. The expression
     * result is boolean if it is true object are not stored in the cache. It has also
     * ability to exclude unless object from the method result {@link #skipUnlessInResult}
     * <p>The SpEL expression evaluates against a dedicated context that provides the
     * following meta-data:
     * <ul>
     * <li>{@code #result} for a reference to the each object of the result result of the
     *  method invocation.</li>
     * <li>{@code #root.method}, {@code #root.target}, and {@code #root.caches} for
     * references to the {@link java.lang.reflect.Method method}, target object, and
     * affected cache(s) respectively.</li>
     * <li>Shortcuts for the method name ({@code #root.methodName}) and target class
     * ({@code #root.targetClass}) are also available.
     * <li>Method arguments can be accessed by index. For instance the second argument
     * can be accessed via {@code #root.args[1]}, {@code #p1} or {@code #a1}. Arguments
     * can also be accessed by name if that information is available.</li>
     * </ul>
     * @return Return {@link java.lang.String}, SPeL expression to calculate condition
     * if object should be stored in cache. If expression is true, object is not put to
     * cache, if false, object is stored to cache.
     */
    String unless() default "";

    /**
     * {@link #skipUnlessInResult} is boolean expression. if it is true, the {@link #unless}
     * objects are excluded from the result collection otherwise result collection
     * contains {@link #unless} objects.
     * Default {@link #skipUnlessInResult} is false.
     * @return Return {@link Boolean}. If true, the object is #unless = true, the
     * object is skipped in final result, otherwise the object put to the result. If #unless]
     * is not set, the #skipUnlessInResult is skipped.
     */
    boolean skipUnlessInResult() default false;
}
