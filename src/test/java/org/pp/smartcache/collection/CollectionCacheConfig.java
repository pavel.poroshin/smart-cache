package org.pp.smartcache.collection;

import org.mockito.Mockito;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@EnableCaching
@Configuration
@EnableAspectJAutoProxy
public class CollectionCacheConfig {
    private static final String CACHE_NAME = "test-cache";

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager(CACHE_NAME) ;
    }

    @Bean
    public ResponseGenerator responseGenerator() {
        // That trick needs for aspect works with mocks.
        ResponseGenerator responseGenerator = Mockito.mock(ResponseGenerator.class);

        when(responseGenerator.generateTestDataList(anyList())).thenCallRealMethod();
        when(responseGenerator.generateTestDataCollection(anyList())).thenCallRealMethod();
        when(responseGenerator.generateTestDataItem(any())).thenCallRealMethod();

        return responseGenerator;
    }

}
