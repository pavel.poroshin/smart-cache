package org.pp.smartcache.collection;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ContextConfiguration;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = {CollectionCacheableAspect.class, CacheService.class})
@ContextConfiguration(classes = {CollectionCacheConfig.class})
class CollectionCacheableProcessorTest {

    private static final String CACHE_NAME = "test-cache";

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    ResponseGenerator responseGenerator;

    @Autowired
    private CacheService cacheServiceTest;

    @BeforeEach
    void beforeEach() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        cache.clear();
    }

    @AfterEach
    void afterEach() {
        Mockito.clearInvocations(responseGenerator);
    }

    @Test
    public void testBaseLineWithList() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        List<Pair<String, String>> requestList =
                List.of(new ImmutablePair<>("left1", "right1"),
                        new ImmutablePair<>("left2", "right2"),
                        new ImmutablePair<>("left3", "right3"));

        cacheServiceTest.testBaseLineWithList("a", 1, requestList);
        var result = cacheServiceTest.testBaseLineWithList("a", 1, requestList);

        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(0))));
        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(1))));
        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(2))));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
    }

    @Test
    public void testBaseLineWithCollection() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        List<Pair<String, String>> requestCollection =
                List.of(new ImmutablePair<>("left1", "right1"),
                        new ImmutablePair<>("left2", "right2"),
                        new ImmutablePair<>("left3", "right3"));

        var result = cacheServiceTest.testBaseLineWithCollection("a", 1, requestCollection);
        result = cacheServiceTest.testBaseLineWithCollection("a", 1, requestCollection);

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, requestCollection.get(0))));
        assertNotNull(cache.get(calculateResultKey("a", 1, requestCollection.get(1))));
        assertNotNull(cache.get(calculateResultKey("a", 1, requestCollection.get(2))));
        verify(responseGenerator, times(1)).generateTestDataCollection(anyCollection());
    }

    @Test
    public void testBaseLineWithArrayList() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        ArrayList<Pair<String, String>> requestList = new ArrayList<>();
        requestList.add(new ImmutablePair<>("left1", "right1"));
        requestList.add(new ImmutablePair<>("left2", "right2"));
        requestList.add(new ImmutablePair<>("left3", "right3"));

        cacheServiceTest.testBaseLineWithArrayList("a", 1, requestList);
        var result = cacheServiceTest.testBaseLineWithArrayList("a", 1, requestList);

        assertSame(ArrayList.class, result.getClass());

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(0))));
        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(1))));
        assertNotNull(cache.get(calculateResultKey("a", 1, result.get(2))));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
    }

    @Test
    public void testBaseLineWithSet() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        Set<Pair<String, String>> requestSet = Set.of(p1, p2, p3);

        cacheServiceTest.testBaseLineWithSet("a", 1, requestSet);
        cacheServiceTest.testBaseLineWithSet("a", 1, requestSet);

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
    }

    @Test
    public void testBaseLineWithHashSet() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        HashSet<Pair<String, String>> requestSet = new HashSet<>();
        requestSet.add(p1);
        requestSet.add(p2);
        requestSet.add(p3);

        cacheServiceTest.testBaseLineWithHashSet("a", 1, requestSet);
        var result = cacheServiceTest.testBaseLineWithHashSet("a", 1, requestSet);

        assertSame(HashSet.class, result.getClass());

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
    }

    @Test()
    public void testConsistencyKeys() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        List<Pair<String, String>> request = List.of(p1);

        Exception ex = assertThrows(IllegalStateException.class, () -> cacheServiceTest.testConsistencyKeys("a", 1, request));

        assert cache != null;
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(1)).generateTestDataItem(any());
    }

    @Test()
    public void testUnless() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        List<Pair<String, String>> request = List.of(p1, p2, p3);

        var result = cacheServiceTest.testUnless("a", 1, request);
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));

        assertEquals(3, result.size());
        var result0 = result.get(0);
        assertNotNull(result0);
        assertEquals("left1", result0.getLeft());
        assertEquals("right1", result0.getRight());

        var result1 = result.get(1);
        assertNotNull(result1);
        assertEquals("left2", result1.getLeft());
        assertEquals("right2", result1.getRight());

        var result2 = result.get(2);
        assertNotNull(result2);
        assertEquals("left3", result2.getLeft());
        assertEquals("right3", result2.getRight());

        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(3)).generateTestDataItem(any());
    }

    @Test()
    public void testUnlessSkipUnlessInResult() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        List<Pair<String, String>> request = List.of(p1, p2, p3);

        var result = cacheServiceTest.testUnlessSkipUnlessInResult("a", 1, request);
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));

        assertEquals(1, result.size());
        var result0 = result.get(0);
        assertNotNull(result0);
        assertEquals("left3", result0.getLeft());
        assertEquals("right3", result0.getRight());

        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(3)).generateTestDataItem(any());
    }

    @Test()
    public void testUnlessSkipUnlessInResultFalse() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        List<Pair<String, String>> request = List.of(p1, p2, p3);

        var result = cacheServiceTest.testUnlessSkipUnlessInResultFalse("a", 1, request);
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));

        assertEquals(3, result.size());
        var result0 = result.get(0);
        assertNotNull(result0);
        assertEquals("left1", result0.getLeft());
        assertEquals("right1", result0.getRight());

        var result1 = result.get(1);
        assertNotNull(result1);
        assertEquals("left2", result1.getLeft());
        assertEquals("right2", result1.getRight());

        var result2 = result.get(2);
        assertNotNull(result2);
        assertEquals("left3", result2.getLeft());
        assertEquals("right3", result2.getRight());

        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(3)).generateTestDataItem(any());
    }

    @Test()
    public void testUnlessNotSetSkipUnlessInResultTrue() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        List<Pair<String, String>> request = List.of(p1, p2, p3);

        var result = cacheServiceTest.testUnlessNotSetSkipUnlessInResultTrue("a", 1, request);

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));

        assertEquals(3, result.size());
        var result0 = result.get(0);
        assertNotNull(result0);
        assertEquals("left1", result0.getLeft());
        assertEquals("right1", result0.getRight());

        var result1 = result.get(1);
        assertNotNull(result1);
        assertEquals("left2", result1.getLeft());
        assertEquals("right2", result1.getRight());

        var result2 = result.get(2);
        assertNotNull(result2);
        assertEquals("left3", result2.getLeft());
        assertEquals("right3", result2.getRight());

        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(3)).generateTestDataItem(any());
    }


    @Test()
    public void testUnlessNotSetSkipUnlessInResultFalse() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        var p3 = new ImmutablePair<>("left3", "right3");
        List<Pair<String, String>> request = List.of(p1, p2, p3);

        var result = cacheServiceTest.testUnlessNotSetSkipUnlessInResultFalse("a", 1, request);

        assert cache != null;
        assertNotNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p2)));
        assertNotNull(cache.get(calculateResultKey("a", 1, p3)));


        assertEquals(3, result.size());
        var result0 = result.get(0);
        assertNotNull(result0);
        assertEquals("left1", result0.getLeft());
        assertEquals("right1", result0.getRight());

        var result1 = result.get(1);
        assertNotNull(result1);
        assertEquals("left2", result1.getLeft());
        assertEquals("right2", result1.getRight());

        var result2 = result.get(2);
        assertNotNull(result2);
        assertEquals("left3", result2.getLeft());
        assertEquals("right3", result2.getRight());

        verify(responseGenerator, times(1)).generateTestDataList(anyList());
        verify(responseGenerator, times(3)).generateTestDataItem(any());
    }

    @Test()
    public void testArgCollectionEmpty() {
        var result = cacheServiceTest.testArgCollectionEmpty("a", 1, Collections.emptyList());

        assertEquals(0, result.size());
        verify(responseGenerator, times(0)).generateTestDataList(anyList());
        verify(responseGenerator, times(0)).generateTestDataItem(any());
    }

    @Test
    public void testArgNull() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        List<Pair<String, String>> requestList =
                List.of(new ImmutablePair<>("left1", "right1"),
                        new ImmutablePair<>("left2", "right2"),
                        new ImmutablePair<>("left3", "right3"));

        var result = cacheServiceTest.testArgNull(null, null, requestList);

        assertNotNull(cache.get(calculateResultKey(null, null, result.get(0))));
        assertNotNull(cache.get(calculateResultKey(null, null, result.get(1))));
        assertNotNull(cache.get(calculateResultKey(null, null, result.get(2))));
        verify(responseGenerator, times(1)).generateTestDataList(anyList());
    }

    @Test()
    public void testArgCollectionNull() {
        var result = cacheServiceTest.testArgCollectionNull("a", 1, null);
        assertNull(result);
    }

    @Test()
    public void testUseKeyConstraint() {
        Cache cache = cacheManager.getCache(CACHE_NAME);
        var p1 = new ImmutablePair<>("left1", "right1");
        var p2 = new ImmutablePair<>("left2", "right2");
        List<Pair<String, String>> request = List.of(p1, p2);

        var result = cacheServiceTest.testUseKeyConstraint("a", 1, request);

        assert cache != null;
        assertNotNull(cache.get(calculateWrongResultKey("a", 1, result.get(0))));
        assertNotNull(cache.get(calculateWrongResultKey("a", 1, result.get(1))));
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNull(cache.get(calculateResultKey("a", 1, p2)));

        result = cacheServiceTest.testUseKeyConstraint("a", 1, request);

        assertNotNull(cache.get(calculateWrongResultKey("a", 1, result.get(0))));
        assertNotNull(cache.get(calculateWrongResultKey("a", 1, result.get(1))));
        assertNull(cache.get(calculateResultKey("a", 1, p1)));
        assertNull(cache.get(calculateResultKey("a", 1, p2)));

        verify(responseGenerator, times(2)).generateTestDataList(anyList());
        verify(responseGenerator, times(4)).generateTestDataItem(any());
    }


    private String calculateResultKey(String arg1, Integer arg2, Triple<String, String, String> triple) {
        return arg1 + "_" + arg2 + "_" + triple.getLeft() + "_" + triple.getRight();
    }

    private String calculateWrongResultKey(String arg1, Integer arg2, Triple<String, String, String> triple) {
        return arg1 + arg2 + triple.getLeft() + triple.getRight();
    }

    private String calculateResultKey(String arg1, Integer arg2, Pair<String, String> pair) {
        return arg1 + "_" + arg2 + "_" + pair.getLeft() + "_" + pair.getRight();
    }
}