package org.pp.smartcache.collection;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResponseGenerator {

    public List<Triple<String, String, String>> generateTestDataList(List<Pair<String, String>> input) {
        return input.stream()
                .map(this::generateTestDataItem)
                .collect(Collectors.toList());
    }

    public Collection<Triple<String, String, String>> generateTestDataCollection(Collection<Pair<String, String>> input) {
        return input.stream()
                .map(this::generateTestDataItem)
                .collect(Collectors.toList());
    }
    public Triple<String, String, String> generateTestDataItem(Pair<String, String> input) {
        return new ImmutableTriple<>(input.getLeft(), input.getLeft() + input.getRight(), input.getRight());
    }
}
