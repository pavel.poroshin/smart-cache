package org.pp.smartcache.collection;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.util.Collections.emptyList;

@Component
public class CacheService {

    @Autowired
    private ResponseGenerator responseGenerator;

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public List<Triple<String, String, String>> testBaseLineWithList(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public Collection<Triple<String, String, String>> testBaseLineWithCollection(String arg1, Integer arg2, Collection<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataCollection(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public ArrayList<Triple<String, String, String>> testBaseLineWithArrayList(String arg1, Integer arg2, ArrayList<Pair<String, String>> arr) {
        return new ArrayList<>(responseGenerator.generateTestDataList(arr));
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public Set<Triple<String, String, String>> testBaseLineWithSet(String arg1, Integer arg2, Set<Pair<String, String>> arr) {
        List<Pair<String, String>> list = new ArrayList<>(arr);
        return new HashSet<>(responseGenerator.generateTestDataList(list));
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public HashSet<Triple<String, String, String>> testBaseLineWithHashSet(String arg1, Integer arg2, HashSet<Pair<String, String>> arr) {
        List<Pair<String, String>> list = new ArrayList<>(arr);
        return new HashSet<>(responseGenerator.generateTestDataList(list));
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + #arg2 + #result.left + #result.right")
    public List<Triple<String, String, String>> testConsistencyKeys(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + #arg2 + #result.left + #result.right", useKeyConstraint = false)
    public List<Triple<String, String, String>> testUseKeyConstraint(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right",
            unless = "#result.left == 'left1' or #result.left == 'left2'",
            skipUnlessInResult = false)
    public List<Triple<String, String, String>> testUnless(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right",
            unless = "#result.left == 'left1' or #result.left == 'left2'")
    public List<Triple<String, String, String>> testUnlessSkipUnlessInResultFalse(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right",
            unless = "#result.left == 'left1' or #result.left == 'left2'",
            skipUnlessInResult = true)
    public List<Triple<String, String, String>> testUnlessSkipUnlessInResult(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right",
            skipUnlessInResult = false)
    public List<Triple<String, String, String>> testUnlessNotSetSkipUnlessInResultFalse(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right",
            skipUnlessInResult = true)
    public List<Triple<String, String, String>> testUnlessNotSetSkipUnlessInResultTrue(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public List<Triple<String, String, String>> testArgCollectionEmpty(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return emptyList();
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public List<Triple<String, String, String>> testArgCollectionNull(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return null;
    }

    @CollectionCacheable(cacheName = "test-cache", key = "#arg1 + '_' + #arg2 + '_' + #arr.left + '_' + #arr.right",
            resultKey = "#arg1 + '_' + #arg2 + '_'+ #result.left + '_' + #result.right")
    public List<Triple<String, String, String>> testArgNull(String arg1, Integer arg2, List<Pair<String, String>> arr) {
        return responseGenerator.generateTestDataList(arr);
    }

}
