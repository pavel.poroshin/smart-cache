# Smart cache

This Java library implements extra cache functionality based on spring cache. There are annotations added:
- @CollectionCacheable

The ideas of the functionality has been described further.

The library can be found in the artifactory: [smart-cache](https://artifactory.devops.telekom.de/artifactory/asf-releases-maven/de/telekom/asf/poq/smart-cache/).

## Configuration
Gradle dependency:
For start using the library follow steps have to be done:
- add spring cache configuration it needs for the project
- add to build.gradle 'implementation "de.telekom.asf.poq:smart-cache:1.0.3"` or the appropriate lines in case of maven.
- add @Import({SmartCacheConfiguration.class}) to the project

## Usage

### @CollectionCacheable
#### Description
@CollectionCacheable is annotation to run around aspect working with cache. The functionality serves when the function has one collection parameter, it heeds to handle each collection item separately.
It might be case when some collection items are already in cache, but other items are not. In this case cached items are taken from the cache other items are sent to the real method.  
When method returns result, result combines with the data taken from the cache in input collection order. Result of the method call is cached for future usage.

@CollectionCacheable attributed:

|Attribute|Mandatory|Description|
|:---:|:---:|:---|
|cacheName|yes|Names may be used to determine the target cache|
|key|yes|_key_ for extract objects from the cache. _SPeL_ is used for generation. All input arguments are available with argument name. In case of collection attribute each item of collection is is available with collection argument name|
|resultKey|yes|Key for putting objects to the cache. _SPeL_ is used for generation. All input arguments are available with argument name. Item of result collection are available with result name. _ResultKey_ must be in the keys (**see Restriction**)|
|unless|no|Boolean expression if it true, result objects are not store in the cache. _SPeL_ is used for generation. All input arguments are available with argument name. All input arguments are available with argument name.
|skipUnlessInResult|no|_skipUnlessInResult_ is boolean expression. if it is true, the _unless_ objects are excluded from the result collection otherwise result collection contains _unless_ objects. Default _skipUnlessInResult_ is false.|
|useKeyConstraint|no|_useKeyConstraint_ is boolean expression. if it is false, keys constrain validation is skipped the warn message is print in log only. Otherwise _IllegalStateException_ is risen. Default _useKeyConstraint_ is true. (**see Restriction**)|

#### Example

For example there is follow method in the application:
```
    @CollectionCacheable(cacheName = "cache-name", key = "#src + #ids",
	        resultKey = "#src + #result.id")
	public List<Item> method(String src, List<String> ids) {...}
```
For example method called with _src="src"_ and _ids={"1", "2", "3"}_. Suppose, the method has been already called with ids={"2"} before, result has been cached.
What happens step by step:
- In this case input arguments are decomposed to _{"src1", "src2", "src3"}_.
- Then each key is checked if it is already in cache or not. Object _item[id="2", value="two"]_ is found in the cache.
- The new collection argument is created without keys are already in cache. As a result we have new arguments to call method _src="src"_, _ids=["1", "3"]_
- Call the method with new arguments and gets result. Method returns _{item[id="1", value="one"]_, _item[id="3", value="three"]}_
- Calculate for each result item resultKey. It must match any input key otherwise _IllegalStateException_ is risen. If everything is ok, result is cached as well.
- Then combine method result with items are in cache. Result is returned as _{item[id="1", value="one"], item[id="2", value="one"], item[id="3", value="three"]}_


#### Important

- If result contains more item for each input _key_, the result object are grouped by _key_ and stored to the cache as a collection of the objects.
  If at least one result item by _key_ is unless, all items for the _key_ is not stored to the cache.
- _Key_ is always _java.lang.String_, so you should care about it for correct work.

#### Restrictions
- It is only one _java.util.collection_ allowed for an input method parameter. if collection is not one the _IllegalArgumentException_ is risen.
- _key_ and _resultKey_ mast by matched to an appropriate result objects. If \#resultKey is not found in #key set then _IllegalStateException_ is risen.

